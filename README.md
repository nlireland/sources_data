# Sources Data #

This repository holds the data for the *Sources Database*. This can be converted to MARC for loading into the public Sources interface using the tool in [https://bitbucket.org/nlireland/sources2marc](https://bitbucket.org/nlireland/sources2marc)

### Directory Structure ###
* **trunk/MS/UR** - The unified records for Manuscripts. These are the records to use in the public interface (other than records in trunk/MS/UR/DeletedRecords)
* **trunk/MS/UR/DeletedRecords** - Not to be loaded into the public interface. These are records that we manually merged with other UR records & are therefore no longer needed
* **trunk/MS/Non-UR** - The source records for each individual Sources Manuscripts entry. These are for reference only. The combined, final versions are in trunk/MS/UR 
* **trunk/PS/UR** - The unified records for periodicals. These are the records to use in the public interface (other than records in trunk/PS/UR/DeletedRecords)
* **trunk/PS/UR/DeletedRecords** - Not to be loaded into the public interface. These are records that we manually merged with other UR records & are therefore no longer needed
* **trunk/PS/Non-UR** - The source records for each individual Sources Periodicals entry. These are for reference only. The combined, final versions are in trunk/PS/UR 
* **trunk/PS/spellchecking** - These are no longer needed. This files were used for carrying out spelling correction. The amendments were merged into the final versions in trunk/PS/UR
* **trunk/SVNTesting** - a directory used for training users in SVN.